import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.net.URL;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Sample code for reading category and URL data.
 */
public class Sample {

    public static void main(String[] args) throws Exception {
        ///////////////////////////////////////////////////////////////////////////
        // Properties in form of String (key) -> String (value)...               //
        // Note that the the value represents a comma separated list of keywords //
        ///////////////////////////////////////////////////////////////////////////
        //Properties properties = loadProperties();
        //List<String> lines = getLines("http://en.wikipedia.org/wiki/Toy");


        final ContextualizerStandard c = new ContextualizerStandard();

        Callable<String> r1 = new Callable<String>() {
            @Override
            public String call() {
                try {
                    return c.contextualize("http://en.wikipedia.org/wiki/Finance", 10000);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        };

        Callable<String> r2 = new Callable<String>() {
            @Override
            public String call() {
                try {
                    return c.contextualize("http://en.wikipedia.org/wiki/Finance", 10000);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        };





        Future f1 = Executors.newFixedThreadPool(10).submit(r1);
        Future f2 = Executors.newFixedThreadPool(10).submit(r2);

        System.out.println(f1.get());
        System.out.println(f2.get());
    }

    private static Properties loadProperties() throws IOException {
        Properties properties = new Properties();
        properties.load(Sample.class.getResourceAsStream("categories.properties"));
        return properties;
    }

    private static List<String> getLines(String path) throws IOException {
        URL url = new URL(path);
        LineNumberReader reader = new LineNumberReader(new InputStreamReader(url.openConnection().getInputStream()));
        List<String> lines = new ArrayList<String>();
        String line = "";
        while (line != null) {
            line = reader.readLine();
            lines.add(line);
        }
        return lines;
    }



}
