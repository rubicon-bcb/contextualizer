import java.util.concurrent.TimeoutException;

/**
 * A contextualizer that returns the top relevant category for a given URL with null if none are found.
 * The categories file (categories.properties) file is stored in the form: [category:keyword,keyword,keyword] with one
 * line per category.
 */
public interface Contextualizer {

    /**
     * Contextualizes a url to a possible category that is defined in the categories.properties file
     *
     * @param url The url to contextualize
     * @return Best matched category based on the data in categories.properties file.
     */
    String contextualize(String url);

    /**
     * Contextualizes a url to a possible category that is defined in categories.properties file.
     * The operation is given a timeout specified in the parameter.
     *
     * @param url The url to contextualize
     * @param timeout Operation timeout
     * @return Best matched category based on the data in categories.properties file.
     * @throws java.util.concurrent.TimeoutException
     */
    String contextualize(String url, int timeout) throws TimeoutException;

}
