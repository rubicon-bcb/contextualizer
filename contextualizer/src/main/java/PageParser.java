import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by avenkatraman on 1/9/15.
 */
public class PageParser implements IPageParser {

    private LineParser lineParser;
    private Downloader downloader;

    public LineParser getLineParser() {
        return lineParser;
    }

    public void setLineParser(LineParser lineParser) {
        this.lineParser = lineParser;
    }

    public Downloader getDownloader() {
        return downloader;
    }

    public void setDownloader(Downloader downloader) {
        this.downloader = downloader;
    }

    @Override
    public Map<String, Integer> parse(String url, Map<String, Set<String>> categories) throws IOException {
        if (url == null) {
            throw new IllegalArgumentException("URL should be provided");
        }
        Map<String, Integer> weights = new HashMap<String, Integer>();
        if (categories == null) {
            return weights;
        }
        List<String> lines = downloader.getLines(url);
        for (String line : lines) {
            if (line == null || "".equals(line)) {
                continue;
            }
            Map<String, Integer> lineWeights = lineParser.parse(line, categories);
            for (String category : lineWeights.keySet()) {
                Integer categoryWeight = weights.get(category);
                if (categoryWeight == null) {
                    weights.put(category, lineWeights.get(category));
                } else {
                    Integer lineWeight = lineWeights.get(category);
                    if (lineWeight == null) {
                        lineWeight = 0;
                    }
                    weights.put(category,lineWeight + categoryWeight);
                }
            }
        }
        return weights;
    }
}
