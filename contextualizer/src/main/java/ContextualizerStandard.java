import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.net.URL;
import java.util.*;
import java.util.concurrent.*;

public class ContextualizerStandard implements Contextualizer {

    private Map<String, Set<String>> categories;

    public ContextualizerStandard() {
        try {
            this.categories = getCategories(loadProperties());
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }

    @Override
    public String contextualize(String url) {
        IPageParser parser = getParser();
        try {
            Map<String, Integer> weights = parser.parse(url, categories);//getPageCategoriesWeight(url);
            Integer max = 0;
            String result = null;
            for (String category : weights.keySet()) {
                Integer categoryWeight = weights.get(category);
                if (categoryWeight != null && categoryWeight > max) {
                    max = categoryWeight;
                    result = category;
                }
            }
            return result;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    private final ExecutorService service = Executors.newFixedThreadPool(10);
    private final ConcurrentMap<String, String> url2category = new ConcurrentHashMap<String, String>();
    private static final String EMPTY = "";

    @Override
    public String contextualize(final String url, int timeout) throws TimeoutException {
        Future<String> f = null;
        String prev = url2category.putIfAbsent(url, EMPTY);

        if (prev != null && !EMPTY.equals(prev)) {
            return prev;
        }
        else if (EMPTY.equals(prev)) {
            f = service.submit(new Callable<String>() {
                @Override
                public String call() throws Exception {
                    String cat = EMPTY;
                    while (EMPTY.equals(cat)) {
                        cat = url2category.get(url);
                    }
                    return cat;
                }
            });
        } else if (prev == null) {
            f = service.submit(new Callable<String>() {
                @Override
                public String call() throws Exception {
                    String cat = contextualize(url);
                    url2category.put(url, cat);
                    return cat;
                }
            });

        }
        String category = null;
        try {
            category = f.get(timeout, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        } catch (TimeoutException e) {
            if (!f.isCancelled() && !f.isDone()) {
                f.cancel(true);
                throw e;
            }
        }
        return category;
    }

    private static Properties loadProperties() throws IOException {
        Properties properties = new Properties();
        properties.load(Sample.class.getResourceAsStream("categories.properties"));
        return properties;
    }

    private static Map<String, Set<String>> getCategories(Properties categories) {
        Map<String, Set<String>> result = new HashMap<String, Set<String>>();
        Enumeration categoriesNames = categories.propertyNames();
        while(categoriesNames.hasMoreElements()) {
            String categoryName = (String)categoriesNames.nextElement();
            String[] words = categories.getProperty(categoryName).split(",");
            Set<String> set = new HashSet<String>();
            for (String word : words) {
                set.add(word);
            }
            result.put(categoryName, set);
        }
        return result;
    }

//    private static List<String> getLines(String path) throws IOException {
//        URL url = new URL(path);
//        LineNumberReader reader = new LineNumberReader(new InputStreamReader(url.openConnection().getInputStream()));
//        List<String> lines = new ArrayList<String>();
//        String line = "";
//        while (line != null) {
//            line = reader.readLine();
//            lines.add(parse(line));
//        }
//        return lines;
//    }
//
//    private static String parse(String line) {
//        //TODO
//        return line;
//    }
//
//
//
//    private Map<String, Integer> getPageCategoriesWeight(String url) throws IOException {
//        if (url == null) {
//            throw new IllegalArgumentException("URL should be provided");
//        }
//        Map<String, Integer> weights = new HashMap<String, Integer>();
//        if (categories == null) {
//            return weights;
//        }
//        List<String> lines = getLines(url);
//        for (String line : lines) {
//            if (line == null || "".equals(line)) {
//                continue;
//            }
//            Map<String, Integer> lineWeights = getCategoriesWeight(line);
//            for (String category : lineWeights.keySet()) {
//                Integer categoryWeight = weights.get(category);
//                if (categoryWeight == null) {
//                    weights.put(category, lineWeights.get(category));
//                } else {
//                    Integer lineWeight = lineWeights.get(category);
//                    if (lineWeight == null) {
//                        lineWeight = 0;
//                    }
//                    weights.put(category,lineWeight + categoryWeight);
//                }
//            }
//        }
//        return weights;
//    }
//
//    private Map<String, Integer> getCategoriesWeight(String line) {
//        Map<String, Integer> weights = new HashMap<String, Integer>();
//        for (String categoryName : categories.keySet()) {
//            weights.put(categoryName, getCategoryWeight(line, categories.get(categoryName)));
//        }
//        return weights;
//    }
//
//    private static int getCategoryWeight(String line, Set<String> words) {
//        int categoryWeight = 0;
//        for (String word : words) {
//            categoryWeight = categoryWeight + getWordCount(line, word);
//        }
//        return categoryWeight;
//    }
//
//    private static int getWordCount(String line, String word) {
//        int count = 0;
//        int index = 0;
//        while (index >= 0 && index <= line.length()) {
//            int wordIndex = line.indexOf(word, index);
//            if (wordIndex >= 0) {
//                count++;
//                index = wordIndex + word.length() - 1;
//            } else {
//                index = -1;
//            }
//        }
//        return count;
//    }

    private IPageParser getParser() {
        PageParser parser = new PageParser();
        parser.setDownloader(new SimpleDownloader());
        LineParser lineParser = new LineParser();
        lineParser.setCounter(new SimpleCounter());
        parser.setLineParser(lineParser);
        return parser;
    }

}
