import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by avenkatraman on 1/9/15.
 */
public class SimpleDownloader implements  Downloader {

    @Override
    public List<String> getLines(String path) throws IOException {
        URL url = new URL(path);
        LineNumberReader reader = new LineNumberReader(new InputStreamReader(url.openConnection().getInputStream()));
        List<String> lines = new ArrayList<String>();
        String line = "";
        while (line != null) {
            line = reader.readLine();
            lines.add(line);
        }
        return lines;
    }
}
