/**
 * Created by avenkatraman on 1/9/15.
 */
public class SimpleCounter implements Counter {

    @Override
    public int count(String line, String word) {
        int count = 0;
        int index = 0;
        while (index >= 0 && index <= line.length()) {
            int wordIndex = line.indexOf(word, index);
            if (wordIndex >= 0) {
                count++;
                index = wordIndex + word.length() - 1;
            } else {
                index = -1;
            }
        }
        return count;
    }
}
