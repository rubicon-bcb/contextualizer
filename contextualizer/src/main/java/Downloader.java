import java.io.IOException;
import java.util.List;

/**
 * Created by avenkatraman on 1/9/15.
 */
public interface Downloader {

    List<String> getLines(String path) throws IOException;
}
