/**
 * Created by avenkatraman on 1/9/15.
 */
public interface Counter {

    int count(String line, String word);
}
